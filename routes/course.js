const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth.js");

// Create course
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

});

// retrieve all courses

router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

/*
	create a route that will retrieve ALL ACTIVE courses
	no need for user to login
	create controller that will return ALL ACTIVE courses
	send to postamn output screenshot and send in batch hangouts
*/

router.get("/allactive", (req, res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController));
})

// retrieve specific
router.get("/:courseId", (req,res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// update a course
router.put("/:courseId", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// archive a course (set to Inactive)
router.put("/archive/:courseId", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.archiveCourse(req.params, data).then(resultFromController => res.send(resultFromController));
})



// export the router object for index.js file
module.exports = router;