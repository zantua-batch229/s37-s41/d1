const Course = require("../models/Course.js");

// Create new course
/*
Steps:
1. Create a new course object using the mongoose model
2. Save the new course to the database
*/

module.exports.addCourse = (data) => {

	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price,
			isActive : data.course.isActive
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {

			// Course creation successful
			if (error) {

				return false;

			// Course creation failed
			} else {

				return true;

			};

		});

	// User is not an admin
	} else {
		return false;
	};
	

};

// retrieve all courses

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result
	});
}

// retrieve all active courses

module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	});
}
// retrieve specific

module.exports.getCourse = (reqParams) => {
	console.log(reqParams);
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
}

// update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the info retrieved from the req body
	2. Find and update the course using the course ID retrieved from the req params and the variable "updatedCourse" 

*/

module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name : reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

		if(error){
			return false;
		} else {
			return true;
		}
	})
}
module.exports.archiveCourse = (reqParams, reqBody) => {
	
	if (data.isAdmin) {

		let ArchiveCourse = new Course({
			isActive : data.course.isActive
		});
		return Course.findByIdAndUpdate(reqParams.courseId, ArchiveCourse).then((archivedCourse, error) => {

			if(error){
				return false;
			} else {
				return true;
			}
		})
	} else {
		return false;
	};
}

